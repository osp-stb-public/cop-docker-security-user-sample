FROM ruby:2.5.1
MAINTAINER Bernd Ledig <bernd.ledig@ottogroup.com>

RUN apt-get update && apt-get upgrade -y

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile .
RUN bundle install --system

RUN useradd --home-dir $PWD --no-create-home --user-group api && chown -R api:api /usr/src/app
USER api

COPY . /usr/src/app

ENV RAILS_LOG_TO_STDOUT true
EXPOSE 3000
CMD rm -f tmp/pids/server.pid && rails server -e $RAILS_ENV
