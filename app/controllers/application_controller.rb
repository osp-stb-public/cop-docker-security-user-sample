class ApplicationController < ActionController::API

  before_action :check_api_key

  def check_api_key
    api_key = request.headers['x-api-key']
    # logger.debug "x-api-key: #{api_key}"
    if APP_CONFIG.api_key != api_key
      logger.warn "Wrong or missing api key! x-api-key= '#{api_key}'"
      render json: {message: "wrong auth!"}, status: :unauthorized
      return false
    end
  end
  
end
