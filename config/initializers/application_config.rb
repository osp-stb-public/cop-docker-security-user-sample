
require 'ostruct'

app_attrs = YAML.load_file("#{Rails.root}/config/application.yml")[Rails.env]
APP_CONFIG = OpenStruct.new(app_attrs) if app_attrs

api_key = ENV['API_KEY']
if api_key.present?
  APP_CONFIG.api_key = api_key
end

Rails.logger.info "APP_CONFIG loaded: #{APP_CONFIG.marshal_dump}"
